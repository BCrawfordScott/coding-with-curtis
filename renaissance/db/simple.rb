module Simple
    SCHEDULES = [
        {
            number: 1
        }
    ]

    SHOWS = [
        {
            title: "Ye Olde Joust",
            number: 1
        },
        {
            title: "Jester's Court",
            number: 2
        },
        {
            title: "Acrobats",
            number: 3
        },
        {
            title: "The King's Court",
            number: 4
        },
        {
            title: "Tavern Tussle",
            number: 5
        },
        {
            title: "Romeo's Riddle",
            number: 6
        },
        {
            title: "Knightly Knife Dance",
            number: 7
        }
    ]

    PERFORMANCES = [
        {
            show: 1,
            start_time: "9:00",
            end_time: "11:00"
        },
        {
            show: 3,
            start_time: "12:00",
            end_time: "13:00"
        },
        {
            show: 5,
            start_time: "15:00",
            end_time: "18:00"
        },
        {
            show: 7,
            start_time: "18:00",
            end_time: "19:00"
        },
        {
            show: 2,
            start_time: "18:00",
            end_time: "19:00"
        },
        {
            show: 4,
            start_time: "18:00",
            end_time: "19:00"
        },
        {
            show: 6,
            start_time: "18:00",
            end_time: "19:00"
        },
    ]
end
