require 'rspec'
require 'show.rb'

describe Show do
    let(:perf1) { Performance.new({start_time: "12:00", end_time: "13:00"})}
    let(:perf2) { Performance.new({start_time: "13:00", end_time: "14:00"})}
    let(:perf3) { Performance.new({start_time: "15:00", end_time: "16:00"})}

    subject(:show) { Show.new({ title: "Tester", performances: [perf1, perf2, perf3] }) }

    describe "::defaults" do
        it "returns a default array of shows" do
            defaults = Show.defaults

            expect(defaults.length).to be(7)
            expect(defaults.all? {|show| show.class == Show}).to be true
        end
    end
    
    describe "#initialize" do
        it "should have a title" do
            expect(subject.title).to eq("Tester")
        end
        it "should have an array of performances" do
            expect(subject.performances).to eq([perf1, perf2, perf3])
        end
    end

    describe "#earliest" do
        it "should return the earliest performance" do
            expect(subject.earliest).to be(perf1)
        end
    end

    describe "#latest" do
        it "should return the latest performance" do
            expect(subject.latest).to be(perf3)
        end
    end

    describe "#earliest_time" do
        it "should return the time of the earliest show" do
            expect(subject.earliest_time).to eq(Time.parse("12:00"))
        end
    end

    describe "#next_available" do
        it "should return the earliest performance starting after the given time" do
            expect(subject.next_available(Time.parse("10:00"))).to be(perf1)
            expect(subject.next_available(Time.parse("13:00"))).to be(perf2)
            expect(subject.next_available(Time.parse("14:00"))).to be(perf3)
        end

        it "should raise a NotTimeError when not given a Time" do
            expect { subject.next_available("13:00") }.to raise_error(NotTimeError)
        end
    end

end