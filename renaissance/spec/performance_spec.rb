require 'rspec'
require 'performance.rb'

describe Performance do
    let(:perf1) { Performance.new({start_time: "12:00", end_time: "13:00"})}
    let(:perf2) { Performance.new({start_time: "13:00", end_time: "14:00"})}
    
    describe "#initialize" do
        it "should have a start time" do
            expect(perf1.start_time).to eq(Time.parse("12:00")) 
        end
        
        it "should have an end time" do
            expect(perf1.end_time).to eq(Time.parse("13:00")) 
        end   
    end
    
    context "when comparing" do
        it "should be comparable" do 
            expect(perf1 < perf2).to be(true)
            expect(perf1 > perf2).to be(false)
            expect(perf1 <= perf2).to be(true)
            expect(perf1 >= perf2).to be(false)
        end

        it "shoould raise an error if compared to something other than a performance" do
            expect { perf1 > 1 }.to raise_error(NotPerformanceError)
        end
    end

end