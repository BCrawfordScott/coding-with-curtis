require 'rspec'
require 'poly_tree_node.rb'

describe PolyTreeNode do
  let(:node1) { PolyTreeNode.new(data: 'root') }
  let(:node2) { PolyTreeNode.new(data: 'child1') }
  let(:node3) { PolyTreeNode.new(data: 'child2') }

  describe "#initialize" do
    let(:new_node) { PolyTreeNode.new(data: 'new_node') }

    it "should set initial data" do
      expect(new_node.data).to eq('new_node')
    end

    it "should set parent to nil" do
      expect(new_node.parent).to be_nil
    end

    it "should set children to an empty array" do
      expect(new_node.children).to eq([])
    end
  end

  describe "#parent=" do
    before do
      node2.parent = node1
      node3.parent = node1
    end

    it "should set a node's parent to the passed node" do
      expect(node2.parent).to equal(node1)
      expect(node3.parent).to equal(node1)
    end

    it "should add the child node to the passed node's children" do
      expect(node1.children).to eq([node2, node3])
    end

    it "does not add the same node twice" do
      node3.parent = node1
      expect(node1.children).to eq([node2, node3])
    end

    it "handles nil without issue" do
      node2.parent = nil
      expect(node2.parent).to be_nil
    end

    context "when reassigning" do
      before { node3.parent = node2 }

      it "should set the node's parent to the new parent" do
        expect(node3.parent).to equal(node2)
      end

      it "should add the node to the new parent's children" do
        expect(node2.children).to include(node3)
      end

      it "should remove the node from its old parent's children" do
        expect(node1.children).to_not include(node3)
      end
    end
  end

  describe "#<<" do

    it "should raise an error if node is not an instance of PolyTreNode" do
        expect { node2 << 'node' }.to raise_error(NotTreeNode)
    end

    it "should pass itself to the child's #parent=" do
      expect(node3).to receive(:parent=).with(node2)
      node2 << node3
    end

    it "doesn't add the child twice" do
      node2 << node3
      expect(node2.children).to eq([node3])
    end
  end

  describe "#remove_child" do
    before do
      node3.parent = node2
    end

    it "should pass nil to the child's #parent=" do
      expect(node3).to receive(:parent=).with(nil)
      node2.remove_child(node3)
    end

    it "should raise an error if node is not an instance of PolyTreNode" do
      expect { node2.remove_child("node") }.to raise_error(NotTreeNode)
    end
  end
end