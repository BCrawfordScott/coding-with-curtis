require 'rspec'
require 'schedule.rb'

describe Schedule do
    let(:perf1) { Performance.new({start_time: "12:00", end_time: "13:00"})}
    let(:perf2) { Performance.new({start_time: "13:00", end_time: "14:00"})}
    let(:perf3) { Performance.new({start_time: "15:00", end_time: "16:00"})}
    let(:perf4) { Performance.new({start_time: "13:00", end_time: "14:00"})}
    let(:perf5) { Performance.new({start_time: "15:00", end_time: "16:00"})}
    let(:perf6) { Performance.new({start_time: "16:00", end_time: "17:00"})}

    let(:show1) { Show.new({ title: "Tester", performances: [perf1, perf2, perf3] }) }
    let(:show2) { Show.new({ title: "Tester2", performances: [perf4, perf5, perf6] }) }

    subject(:schedule) { Schedule.new({ shows: [show1, show2]})}

    describe "::default" do
        it "should generate a single schedule with shows" do
            default = Schedule.default

            expect(default.class == Schedule).to be true
            expect(default.shows.length).to be 7
        end
    end

    describe "#initialize" do
        it "should have an array of shows" do
            expect(subject.shows).to eq([show1, show2])
        end
    end

    describe "#earliest" do
        it "should return the shows with the earliest performances" do
            expect(subject.earliest).to eq([show1])
        end
    end
    
    describe "#latest" do
        it "should return the shows with the latest performances" do
            expect(subject.latest).to eq([show2])
        end
    end
    
    describe "#next_available" do
        it "should return the shows with the earliest performances after a given time" do
            expect(subject.next_available(Time.parse("15:00"))).to eq([show1, show2])
            expect(subject.next_available(Time.parse("16:00"))).to eq([show2])
        end

        it "should return the earliest occuring shows if not given a Time" do
            expect(subject.next_available).to eq([show1])
        end

        it "should raise a NotTimeError if given something other than a Time" do
            expect { subject.next_available(34) }.to raise_error(NotTimeError)
        end
    end
end