# PolyTreeNode API
# initialize(params) -> create a new node
## params = Hash{parent: optional//node, children: optional//Array[nodes], data: any}
# << -> add a child
# parent = (node//node) -> change a node's parent
# remove_child(node//node) -> remove a child form children
# height -> return an intenger representing the number of nodes from current to furthest child
# tallest_children -> return an Array of child nodes having the tallest height
# children -> Array of child nodes
# parent -> parent node
# data -> Any value assigned to the node



class PolyTreeNode

    attr_reader :children, :data, :parent

    def initialize(params)
        @parent = params[:params]
        @data = params[:data]
        @children = params[:children] || []
    end

    def <<(node)
        check_node(node)

        add_child(node)
    end

    def parent=(new_parent)
        return if self.parent == new_parent

        parent.children.delete(self) if parent

        @parent = new_parent
        new_parent.children << self unless new_parent.nil?
        
        self
    end

    def remove_child(node)
        check_node(node)

        node.parent = nil if children.include?(node)
    end

    def delete
        self.parent = nil
    end

    def height
        # debugger
        return 1 if parent.nil?

        return 1 + parent.height
    end

    # def tallest_children
    #     # debugger
    #     max = tallest_child
    #     children.select { |child| child.height == max }
    # end

    # def trim!
    #     return if children.all? { |child| child.children.empty? }

    #     children.reject! { |child| child.children.empty? }

    #     children.each(&:trim!)
    # end
    
    def print_lineage
        puts "\nhere's a plan!"
        build_lineage.each do |node|
            puts "See #{node.data[:show].title}"
            puts "from #{node.data[:performance].start_time} to #{node.data[:performance].end_time} "
        end
    end

    # def parents
    #     return [] if parent.nil?
    #     [parent].concat(parent.parents)
    # end

    def build_lineage
        return [self] if parent.nil?

        return  parent.build_lineage + [self]
    end

    def inspect
        puts "#{data[:show]} - #{data[:performance]}"
    end
    private

    def add_child(node)
        node.parent = self
    end

    def check_node(node)
        raise NotTreeNode unless node.class == self.class
    end

    # def child_heights
    #     # debugger
    #     children.map(&:height)
    # end

    # def tallest_child
    #     # debugger
    #     child_heights.max
    # end

end

class NotTreeNode < StandardError

    def message
        "Must be an instance of PolyTreeNode"
    end
    
end
