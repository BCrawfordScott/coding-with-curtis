require "time"

class Performance

    include Comparable

    attr_reader :start_time, :end_time

    def initialize(params)
        @start_time = Time.parse(params[:start_time])
        @end_time = Time.parse(params[:end_time])
    end

    def <=>(performance)
        raise NotPerformanceError unless performance.class == self.class

        start_time <=> performance.start_time
    end

    def inspect
        puts "#{start_time} : #{end_time}"
    end

end

class NotPerformanceError < StandardError
    def message
        "Can only compare to other performances"
    end
end 
