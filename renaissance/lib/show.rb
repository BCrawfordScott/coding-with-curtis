require_relative './performance.rb'
require_relative '../db/sample.rb'
require_relative '../db/simple.rb'

class Show

    attr_reader :performances, :title

    def self.defaults
        performances = Sample::PERFORMANCES

        Sample::SHOWS.map do |show_data|
            show_performances = performances
                                    .select { |p| p[:show] == show_data[:number] }
                                    .map { |p| Performance.new(p) }

            new(show_data.merge({ performances: show_performances }))
        end
    end

    def self.simple
        performances = Simple::PERFORMANCES

        Simple::SHOWS.map do |show_data|
            show_performances = performances
                                    .select { |p| p[:show] == show_data[:number] }
                                    .map { |p| Performance.new(p) }

            new(show_data.merge({ performances: show_performances }))
        end
    end

    def initialize(params)
        @title = params[:title]
        @performances = params[:performances].sort.freeze
    end

    def earliest
        performances.first
    end

    def latest
        performances.last
    end

    def earliest_time
        earliest.start_time
    end

    def latest_time
        latest.start_time
    end

    def next_available(time)
        raise NotTimeError unless time.class == Time
        
        performances.find { |perf| perf.start_time >= time}
    end

    def inspect
        puts "{#{self.class}#{self.object_id} - #{self.title}}"
    end

end

class NotTimeError < StandardError
    def message
        "Must be an instance of Time"
    end
end
