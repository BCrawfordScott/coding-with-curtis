require_relative './poly_tree_node.rb'
require_relative './schedule.rb'
require 'byebug'

class Scheduler
    
    attr_reader :schedule, :best_plans

    def self.most_shows(schedule = nil)
        scheduler = new(schedule)

        scheduler.plan_from_starts
    end
    
    def initialize(schedule = "sample" )
        @schedule = 
        case schedule
        when "sample" 
            Schedule.default
        when "simple"
            Schedule.simple
        else
            Schedule.default
        end
        @best_plans = nil
    end

    def plan_from_starts
        # debugger
        # potential_paths.each(&:trim!)

        # # Generate the longest paths from every starting show
        # longest_paths = potential_paths.(&:trim!)
        # debugger
        # max_shows = potential_paths.map(&:height).max
        # debugger
        count_paths
    end

    def print_plan(node)
        if node.children.empty?
            node.print_lineage
        else
            node.children.each { |node| print_plan(node) }
        end
    end

    # def collect_heights

    private

    def first_shows
        @first_shows ||= schedule.shows.map do |show|
            title = show.title
            performance = show.earliest
            PolyTreeNode.new(data: { show: show, performance: performance })
        end
    end

    def count_paths
        @path_counts = Hash.new { |h, k| h[k] = [] }
        potential_paths.each do |path|
            queue = [path]
            until queue.empty?
                node = queue.shift
                if node.children.empty?
                    @path_counts[node.height] << node
                else
                    queue = queue + node.children
                end
            end
        end

        max = @path_counts.keys.max

        @path_counts[max].each { |node| print_plan(node) }
    end

    def potential_paths
        #start with the a list of each show    
        @potential_paths ||= first_shows.each { |first_show| build_paths(first_show) }
        # debugger
    end

    def build_paths(node)
        # establish a nodes queue (breadth first build) 
           # place the earliest performance for that show into a node as a root
           # find the list of the next earliest shows that can be seen from that node, excluding shows that have been seen 
           # for each of the next earliest shows
                # turn the show + performance into a node
                # assign it as a child of the previous show + time node
                # enqueue these nodes for future review
            # continue through the queue
        queue = [node]
          
        until queue.empty?
            show_node = queue.shift
            next_showtime = show_node.data[:performance].end_time
            seen_shows = show_node.build_lineage.map { |node| node.data[:show] }
            # debugger
            remaining_shows = schedule.shows.reject { |show| seen_shows.include?(show) }

            remaining_shows.each do |show|
                next_performance = show.next_available(next_showtime)
                next if next_performance.nil?

                new_node = PolyTreeNode.new(data: { show: show, performance: next_performance })
                show_node << new_node
                queue << new_node
            end
        end
    end 

    # def potential_paths(mode = 'first')
    #     if mode == 'first' # allows for different paths in the future
    #         @potential_paths ||= first_shows.each do |show_node|
    #             queue = [show_node]
    #             # seen_shows = []
    #             next_showtime = nil

    #             until queue.empty?
    #                 last = queue.shift
    #                 show = last.data[:show]
    #                 seen_shows = last.parents.map do |node|
    #                     node.data[:show]
    #                 end
    #                 # debugger
    #                 next_showtime = last.data[:performance].end_time

    #                 possible_shows = schedule.next_available(next_showtime)
    #                                     .select { |s| !seen_shows.include?(s) }

    #                 possible_shows.each do |next_show|

    #                     next if show.next_available(next_showtime).nil?
    
    #                     new_node = PolyTreeNode.new(data: {
    #                         show: next_show, 
    #                         performance: show.next_available(next_showtime)
    #                     })

    #                     new_node.parent = last

    #                     queue << new_node
    #                 end
    #             end
    #         end
    #     end
    # end

end
