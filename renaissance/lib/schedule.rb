require_relative './show.rb'
require_relative './show.rb'

class Schedule

    attr_reader :shows

    def self.default
        new({ shows: Show.defaults })
    end

    def self.simple
        new({ shows: Show.simple })
    end

    def initialize(params)
        @shows = params[:shows].freeze
    end

    

    def earliest
        shows.sort { |show1, show2| show1.earliest <=> show2.earliest }
            .filter { |show| show.earliest_time == shows[0].earliest_time }
    end

    def latest
        shows.sort { |show1, show2| show1.latest <=> show2.latest }
            .filter { |show| show.latest_time == shows[-1].latest_time }
    end

    def next_available(time = nil)
        return earliest unless time
        raise NotTimeError unless time.class == Time

        shows.select { |show| show.next_available(time) }
    end

end

# for each show
# start with the ealiest performance - make a node
# idetify all the "next possible" for the other shows
# turn those into nodes with parent pointing to the node
