# Renaissance Scheduler Planning
## Prompt
Given any number of shows, all with verying start and end times and verying frequency of show times...

What are the max number of unique shows I can see with a given schedule?

## Sample Schedule
![schedule](assets/schedule.png)

## Approaches

### Algo 1 "Graph Builder/Path Finder"
+ Start with the shows that have the earliest start times
+ All shows are in a list of "left to see"
+ These are the "current available shows"
+ For each currently available show
    + Set the "next available time" to the end time of the show - effectively "block off the time
    + Remove that show from the list of shows left to see
    + Of the remaining shows, find the earliest next available
    + Attach each of those next available as "children" of the current show
    + Repeat algo for each of those children
+ Examine each presented pathway (graph) of possible show combinations for the longest
+ return the longest list(s) as the most possible views
+ Base Case: no currently available shows

### Algo 2 "Make Change/Compare as we go"
+ Take all the shows
+ Establish base cases: retuning nil if there's no "next shows"
+ Define first available shows as the earliest starting shows
+ Establish a "most number seen" variable
+ For each available show
    + Find the next available shows
    + Run algo on each of them
    + for each return value
        + If it's nil, skip
        + Find the longest options in the list and connect them as linked lists to the current available show
+ Find the longest lists of shows from the first availables and return those
+ Similar to 1, but discards as it goes to conserve memory (rather than making a bunch of otherwise useless nodes/graphs)

### Algo 3 "I know my shows"
+ Given "must see" shows/times
+ determine the order of the shows
+ Find the most possible shows to see between each
+ Find the most possible shows to see before the earliest
+ find the most possible shows to see after the latest
+ this is more of an optimization of the first 2 options

## Domain

### Scheduler
*the goal
* Given a schedule
* Can you find the most possible shows to see
* Can also do so with defined "must sees"

-> Implements Node for Linked List

-> Node data: Show, start time, end time

### Schedule
* Has many shows
+ **earliest** : list of shows
+ **latest** : list of shows
+ **shows** : list of all shows
+ **next_available(given_time)** : list of earliest shows starting after a given time. Default to **earliest**

### Shows
* Has many performances
+ **earliest** : performance
+ **latest** : performance with latest start time
+ **performances** : list of performances
+ **next_available(given_time)** : first performance starting after a given time
+ **earliest_itme** : performance time

-> comparable via performances

### Performances
+ **start_time**
+ **end_time**

-> comparable


## Road Map

1. Implement classes w/tests:
    * Performance
    * Show
    * Schedule
    * PolyTreeNode
    * Scheduler
2. Generate algorithm for method 1 "Graph Path Finder"
3. Optimize with Algo 2
4. Expand with Algo 3


## Review with Curtis

### Traveling salesman problem
In general: 
Find a path through a series of connected nodes (A to B)
0(n!)
In ren faire, we assign a postive or negative edge value
A TSP with weights

You want to research "shortest/longest path"
astar
djykstra's

Organizing data
*Modeling Graphs*
Curtis would not use nodes and edges
He would use "adjacency lists" (tree with hashes and arrays)
Ask yourself, "Do I need the ornamentation of a full class?"
Adjacency matrices are ONLY GOOD for small dense graphs (lots of edges)
    A complete graph has edges between all nodes
Building decision states and solving the problem are two different phases


Optimal strategy
"Next soonest ending show algo"
find the next **__ending__** show