# Renaissance

Say you're going to a Renaissance Faire where there are a variety of different shows that you can watch while drinking mead from a sheep's horn and eating a large turkey leg. You determine that to get the most for your money, you will attend as many distinct performances as possible, you want to go see every show possible.

You get a schedule for the shows and try to figure out which shows and at what times you can see them to see as many different shows as possible.

Think and write down three different strategies that you could use to build an algorithm to determine which show to watch. For example, one strategy could be "Last Show Possible" which means that you choose the last show possible for each of the shows, building your schedule from top to bottom of the list.

Once you have three different strategies, write the implementation of what you think is the best one.

Remember that you want a general heuristic (or algorithm, if it's totally the best), one that can handle different intervals like you see below where the shows could have two different schedules depending on the day or month or whatever. It should be able to handle all possible versions of a schedule.
