const points = require('../db/sample.json');

const xVals = {};
const yVals = {};

// function convexFilter(concave) {
//     const ordered = concave.sort(sortCoors);

//     console.log(ordered);
// }

// function sortCoors(coorA, coorB) {
//     [x1, y1] = coorA;
//     [x2, y2] = coorB;

//     if (y1 < y2) {
//         return -1;
//     } else if (y1 > y2) {
//         return 1;
//     } else {
//         if (x1 < x2) {
//             return -1;
//         } else if (x1 > x2) {
//             return 1;
//         } else {
//             return 0;
//         }
//     }
// }

function concaveFilter(arr) {
    // given an array of arrays with x, y coordinates
    // for each pair of coordinates
        // for each x value, find the min and max y
        // for each y value find the min and max x
    // take the min max values, and keep all the points that appear in both lists.
    arr.forEach( coor => {
        recordX(coor);
        recordY(coor);
    })

    return Array.from(intersection(xToSet(), yToSet()));   
}

function recordX(coor) {
    [x, y] = coor
    currentX = xVals[x];

    if (!currentX) {
        xVals[x] = {
            min: coor,
            max: coor
        }
    } else {
        if (currentX.min[1] > y) {
            currentX.min = coor
        }

        if (currentX.max[1] < y) {
            currentX.max = coor
        }
    }
}

function recordY(coor) {
    [x, y] = coor
    currentY = yVals[y];

    if (!currentY) {
        yVals[y] = {
            min: coor,
            max: coor
        }
    } else {
        if (currentY.min[0] > x) {
            currentY.min = coor
        }

        if (currentY.max[0] < x) {
            currentY.max = coor
        }
    }
}

function intersection (setA, setB) {
    const intersect = new Set();
    for (let el of setA) {
        if (setB.has(el)) {
            intersect.add(el);
        }
    }

    return intersect;
}

function xToSet() {
    const xSet = new Set();

    for (let k in xVals) {
       xSet.add(xVals[k].min)
       xSet.add(xVals[k].max)
    }

    return xSet;
}

function yToSet() {
    const ySet = new Set();

    for (let k in yVals) {
       ySet.add(yVals[k].min)
       ySet.add(yVals[k].max)
    }

    return ySet;
}

function findStart(arr) {
    let lowest;

    arr.forEach((coor) => {
        if (!lowest) {
            lowest = coor;
        }
        [lx, ly] = lowest;
        [x, y] = coor;

        if (y < ly || (y === ly && x > lx)) {
            lowest = coor;
        }
    })

    return lowest;
}

function calcVector(x, y) {
    const [x1, y1] = x;
    const [x2, y2] = y;

    return [x2 - x1, y2 - y1];
}

function getCross(coor1, coor2) {
    [x1, y1] = coor1;
    [x2, y2] = coor2;

    return x1 * y2 - y1 * x2;
}

function crossCompare(coor1, coor2, coor3) {
    const vect1 = calcVector(coor2, coor1)
    const vect2 = calcVector(coor2, coor3)

    const cross = getCross(vect1, vect2)

    if (cross < 0) return -1;
    if (cross > 0) return 1;
    return cross;
}

function anticlockSort(coor1, points) {
    const toSort = points.slice();
    toSort.splice(points.indexOf(coor1), 1);
    const sorted = toSort.sort((coor2, coor3) => {
        return crossCompare(coor1, coor2, coor3);    
    })
    return [coor1].concat(sorted);
}



class Stack {
    constructor() {
        this.stack = [];
    }

    push(el) {
        return this.stack.push(el);
    }

    pop() {
        return this.stack.pop();
    }

    peek(num) {
        if (!num) return this.stack[this.stack.length - 1];

        return this.stack.slice(this.stack.length - num , this.stack.length )
    }

    empty() {
        return this.stack.length === 0;
    }

    height() {
        return this.stack.length;
    }

    toArray() {
        return this.stack.slice();
    }
}

function convex (arr) {
    const stack = new Stack();
    const start = arr.slice(0, 3);
    const rest = arr.slice(3, arr.length);
    
    start.forEach(coor => stack.push(coor));

    rest.forEach(coor => {
        let ult = stack.pop();
        let penult = stack.pop();

        let turn = crossCompare(penult, ult, coor);
        
        while (turn > 0) {
            ult = penult;
            penult = stack.pop();

            turn = crossCompare(penult, ult, coor);
        }

        stack.push(penult);
        stack.push(ult);
        stack.push(coor);


    })

    return stack.toArray();
}

function findConvexHull(givenPoints) {
    const concavePoints = concaveFilter(givenPoints);
    const lowest = findStart(concavePoints);
    const sorted = anticlockSort(lowest, concavePoints);
    console.log(convex(sorted));
}

// findConvexHull(points);

//////////////////// Jarvis March

function genSlope(coor1, coor2) {
    [x1, y1] = coor1;
    [x2, y2] = coor2;
    const ySlope = (y1 - y2);
    const xSlope = (x1 - x2);

    return xSlope / ySlope;
}

function findLeftmost(points) {
    let leftmost = points[0];

    points.slice(1).forEach(point => {
        const [lx, ly] = leftmost;
        const [px, py] = point;

        if (px < lx) {
            leftmost = point;
        }
        
        if (px === lx && py < ly) {
            leftmost = point;
        }
    });

    return leftmost;
}

function findGreatestSlope(pointA, points) {
    // For a given collection of points, find the point with the greates slope to a specified point

    let greatest = points[0]
    let greatestSlope = genSlope(pointA, greatest);
    
    points.slice(1).forEach(point => {
        const currSlope = genSlope(pointA, point);
        if (currSlope > greatestSlope) {
            greatest = point;
            greatestSlope = currSlope;
        }
    });

    return greatest;
}

function jarvis(leftmost, points) {
    const remainder = points.slice();
    const result = [leftmost];

    let current = result[result.length - 1];
    let greatest = findGreatestSlope(current, remainder);
    result.push(greatest);
    remainder.splice(remainder.indexOf(greatest), 1);

    while(!(result[result.length - 1] === leftmost)) {
        current = result[result.length - 1];
        greatest = findGreatestSlope(current, remainder);
        result.push(greatest);
        remainder.splice(remainder.indexOf(greatest), 1);
    }

    return result;
}
const leftMost = findLeftmost(points);

console.log(jarvis(leftMost, points));