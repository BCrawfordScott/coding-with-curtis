# Convex Hull

Sometimes, a problem seems really hard. 

Here's the "find a convex hull" problem which means "find the edges between a set of points in which all other points are "inside" the polygon created by those edges.

Check out the attached image.

![schedule](assets/convex-hull.png)

How can you even solve this? What's the starting point?
In a lot of cases, if you start thinking about how you can sort the input data, then you end up with a strategy for creating an algorithm that can solve the problem at hand. Here are some leading questions for you.
+ How can you sort a set of points? What is there that you can sort on?
+ Once you have the points sorted, is there a series of repeatable steps that you can perform as you consider the next points in the sorted list?